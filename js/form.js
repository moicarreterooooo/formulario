document.querySelector('button.btn').addEventListener('click', (e) => {
   /*
   Hacer la validación de que el check de las condiciones legales se ha marcado
   también podéis hacer que esté deshabilitado el botón hasta que se marque las
   condiciones legales
   */
   e.preventDefault();
   document.getElementById('formulario-registro').submit();
});